<?php

class MyClass {
    public function __construct() {
        
    }
    public function AddTwoNumbers($num1, $num2){
        return $num1 + $num2;
    }

    //3)Assign letter grades based on points earned. Using if/else/elseif statements, create a function that returns a letter grade based on the following point breakdowns when called:
    //100-90=A, 80-89=B, 79-70=C, 69-60=D, <60=F
    public function AssignLetterGrade($num) {
        if($num >= 90) {
            return "A";
        } elseif ($num >= 80 and $num < 90) {
            return "B";
        } elseif($num >= 70 and $num < 80) {
            return "C";
        } elseif($num >= 60 and $num < 70) {
            return "D";
        } else {
            return "F";
        }
    }
}

$myClass = new MyClass();

echo $myClass->AddTwoNumbers(5, 7);

echo "<br/><br/>";

//Set variables for your name ($name as a string), and age ($age as an integer).
$name = "Garrett";
$age = 31;

//Create an array $person that contains index 0 as name, and index 1 as age. %10
//Also, create associative key/value pairs within $person that contain name and age. %20
$person = ["name" => $name, "age" => $age];

//Output to browser: "My name is name and age is age."
//1. Echo out $name and $age using double quotes (%5)
echo "My name is $name and my age is $age.<br />";
//2. Echo out $name and $age using single quotes (%5)
echo 'My name is ' . $name . ' and my age is ' . $age . '.<br />';
//3. Echo out $name and $age using $personindex 0 and 1 (%10)
$personValues = array_values($person);
echo "My name is $personValues[0] and my age is $personValues[1].<br/>";
//4. Echo out name and age using key/value pairs in$person (%10)
echo "My name is {$person["name"]} and my age is {$person["age"]}.<br />";

echo "<br/>";

//5. Set the $age to null and echo out age – what is the result? (%10)
//The result: Nothing is printed.
$age = null;
echo $age;
//6. unset() the $name variable, and echo out name – What is the result? (%10)
//The result: an error that the variable is undefined.
unset($name);
//echo $name;

echo "Grade of 94 is an " . $myClass->AssignLetterGrade(94) . "<br/>";
echo "Grade of 54 is an " . $myClass->AssignLetterGrade(54) . "<br/>";
echo "Grade of 89.9 is a " . $myClass->AssignLetterGrade(89.9) . "<br/>";
echo "Grade of 60.01 is a " . $myClass->AssignLetterGrade(60.01) . "<br/>";
echo "Grade of 102.1 is an " . $myClass->AssignLetterGrade(102.1) . "<br/>";

echo "<br/>";

$colors = [
    0 => "Red",
    1 => "Pink",
    2 => "Green",
    3 => "Lime",
    4 => "Blue",
    5 => "Sky Blue",
    6 => "Purple",
    7 => "Indigo",
    8 => "Orange",
    9 => "Dark Orange"
];

foreach ($colors as $key => $value) {
    echo "Color $key is $value.<br/>";
}

?>