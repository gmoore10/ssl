<?php

$config = array(
    'defaultController' => 'home',
    'dbname' => 'fruits',
    'dbpass' => 'password',
    'dbuser' => 'db_user',
    'baseurl' => 'http://127.0.0.1',
    'navigation' => array(
        "home" => "Home",
        "about" => "About",
        "api/showApi" => "API",
        "googleLogin/login" => "Google Login",
        "home/contact" => "Contact",
        "login" => "Login"
    )
);

$str = $config["baseurl"]."/".$_SERVER['REQUEST_URI'];

$urlPathParts = explode('/', ltrim(parse_url($str, PHP_URL_PATH), '/'));

include 'router.php';

$route = new router($urlPathParts, $config);
 
?>