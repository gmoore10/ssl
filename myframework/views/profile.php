<div class="row panel">
    <div class="col-md-12 col-xs-12">
        <img src="/assets/12184061_10156045669530276_7177370258397199081_o.jpg" class="img-thumbnail picture hidden-xs" /><br />
        <form action="/profile/update" method="post" enctype="multipart/form-data">
            <label class="btn btn-primary" style="width: 110px;">Browse
                <input name="img" type="file" style="display: none;" />
            </label>
            <input type="submit" value="Update" class="btn btn-primary" />
        </form>

        <div class="header">
            <h1><?php echo $_SESSION["profile"]["email"] ?></h1>
        </div>
    </div>
</div>