    <main role="main" class="container">
    <?php

function create_image($cap) {
    unlink("./assets/image1.png");
    global $image;
    $image = imagecreatetruecolor(200, 50) or die("Cannot Initialize new GD image stream");
    $background_color = imagecolorallocate($image, 255, 255, 255);
    $text_color = imagecolorallocate($image, 0, 255, 255);
    $line_color = imagecolorallocate($image, 64, 64, 64);
    $pixel_color = imagecolorallocate($image, 0, 0, 255);
    imagefilledrectangle($image, 0, 0, 200, 50, $background_color);
    for ($i = 0; $i < 3; $i++) {
    imageline($image, 0, rand() % 50, 200, rand() % 50, $line_color);
    }

    for ($i = 0; $i < 1000; $i++) {
    imagesetpixel($image, rand() % 200, rand() % 50, $pixel_color);
    }

    $text_color = imagecolorallocate($image, 0, 0, 0);
    ImageString($image, 22, 30, 22, $cap, $text_color);

    $_SESSION["cap"] = $cap;

    imagepng($image, "./assets/image1.png");
}

create_image($data["cap"]);
?>
        <form action="/home/contactRecv" method="POST">
            <div class="form-group">
                <label for="exampleInputEmail1">Email Address</label>
                <input name="email" type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" />
                <?php 
                    if(array_key_exists("validEmail", $data) && $data["validEmail"] == false) { echo "Invalid Email"; }
                ?>
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Password</label>
                <input name="password" type="password" class="form-control" id="exampleInputPassword1" placeholder="Password" />
                <?php 
                    if(array_key_exists("validPassword", $data) && $data["validPassword"] == false) { echo "Please enter a valid password. Please only use upper and lower case alphabetical characters."; }
                ?>
            </div>
            <div class="form-group">
                <label for="exampleSelect2">Example multiple select</label>
                <select name="exampleSelect" multiple class="form-control" id="exampleSelect2">
                    <option>1</option>
                    <option>2</option>
                    <option>3</option>
                    <option>4</option>
                    <option>5</option>
                </select>
            </div>
            <div class="form-group">
                <label for="exampleTextArea">Example textarea</label>
                <textarea name="textarea" class="form-control" id="exampleTextArea" rows="3"></textarea>
            </div>
            <div class="form-check">
                <label class="form-check-label">
                    <input class="form-check-input" type="checkbox" value="">
                    Checkbox
                </label>
            </div>
            <div class="form-check">
            <label class="form-check-label">
              <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios1" value="option1" checked>
              Option 1
            </label>
          </div>
          <div class="form-check">
            <label class="form-check-label">
              <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios2" value="option2">
              Option 2
            </label>
          </div>
          <div class="form-check disabled">
            <label class="form-check-label">
              <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios3" value="option3" disabled>
              Not Available
            </label>
          </div>
          <?php echo "<img src='/assets/image1.png'>"; ?>
          <div>
            <label for="captcha">Enter Captcha </label>
            <input name="captcha" type="captcha" id="captcha"  placeholder="" />
            </div>

            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </main><!-- /.container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="/assets/js/bootstrap.bundle.min.js"></script>
    <script>
      $('.carousel').carousel();

      $("#modalButton").on("click", function() {
        $('#myModal').modal('toggle')
      });

      $(function () {
        $('[data-toggle="popover"]').popover()
      })
    </script>