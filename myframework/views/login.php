    <main role="main" class="container">
    <form>
        <div class="form-group">
        <label for="exampleInputEmail1">Email address</label>
        <input id="email" type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
        <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
        </div>
        <div class="form-group">
        <label for="exampleInputPassword1">Password</label>
        <input id="password" type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
        </div>
        <input id="btnLogin" type="button" class="btn btn-primary" value="Log In">
    </form>
    </main><!-- /.container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="/assets/js/bootstrap.bundle.min.js"></script>

<script>
      $("#btnLogin").on("click", function() {
        $.ajax({
          method: "POST",
          url: "/home/login",
          data: {
            email: $("#email").val(),
            password: $("#password").val()
          },
          success: function(msg) {
            if(msg=="welcome") {
              alert("Login Successful");
            } else {
              alert("Login Not Successful");
            }
          }
        });
      });
    </script>