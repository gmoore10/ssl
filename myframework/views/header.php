<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?php echo $data["pagename"] ?></title>

    <!-- Bootstrap core CSS -->
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="/assets/css/starter-template.css" rel="stylesheet">
  </head>
  <body>

    <nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
      <a class="navbar-brand" href="#">Navbar</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav mr-auto">
        <?php
              foreach($data["config"] as $key => $value) {
                  if($key == $data["pathparts"][0]) {
                    echo '<li class="nav-item active"><a class="nav-link" href=/'. $key . '>' . $value . '<span class="sr-only">(current)</span></a></li>';
                  } else {
                  echo '<li class="nav-item"><a class="nav-link" href=/'. $key . '>' . $value . '</a></li>';
                  }
              }
        ?>
        </ul>
        <div id="navbar" class="navbar navbar-collapse collapse">
          <span style="color: red"><?php echo @$_REQUEST["msg"] ? $_REQUEST["msg"] : ""; ?></span>
          <?php if(@$_SESSION["loggedin"] && @$_SESSION["loggedin"] == 1) { ?>
          <form class="navbar-form navbar-right">
            <a href="/profile">Profile</a> |
            <a href="/auth/logout">Logout</a>
          </form>
          <?php } else { ?>
          <form class="form-inline my-2 my-lg-0" method="post" action="/auth/login">
            <div class="form-group">
              <input type="text" class="form-control mr-sm-2" name="username" placeholder="Username" />
            </div>
            <div class="form-group">
              <input type="password" class="form-control mr-sm-2" name="password" placeholder="Password" />
            </div>
            <button type="submit" class="btn btn-outline-success my-2 my-sm-0">Sign In</button>
          </form>
          <?php } ?>
        </div>


      </div>
    </nav>