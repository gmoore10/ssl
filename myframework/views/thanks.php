    <main role="main" class="container">

    <div class="jumbotron">
        <h1 class="display-3">Thanks!</h1>
        <p class="lead">We will reach out to you shortly.</p>
        <hr class="my-4">
        <p class="lead">
            <a class="btn btn-success btn-lg" href="/" role="button">Go Home</a>
        </p>
    </div>

    </main><!-- /.container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="/assets/js/bootstrap.bundle.min.js"></script>
    <script>
      $('.carousel').carousel();

      $("#modalButton").on("click", function() {
        $('#myModal').modal('toggle')
      });

      $(function () {
        $('[data-toggle="popover"]').popover()
      })
    </script>
  </body>