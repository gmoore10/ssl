<main role="main" class="container">
  <div class="starter-template">
    <h1>Bootstrap starter template</h1>
    <p><a href="/about/showAddForm">Add New</a></p>
    <table class="table table-bordered table-dark">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Name</th>
      <th scope="col">Actions</th>
    </tr>
  </thead>
  <tbody>
    <?php
      foreach($data as $fruit) {
        echo "<tr><th scope='row'>".$fruit["id"]."</th><td>".$fruit["name"]."</td><td><a href='/about/edit?id=".$fruit["id"]."'>Edit</a> | <a href='/about/delete?id=".$fruit["id"]."'>Delete</a></td></tr>";
      }
    ?>
  </tbody>
</table>
  </div>
</main><!-- /.container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="/assets/js/bootstrap.bundle.min.js"></script>