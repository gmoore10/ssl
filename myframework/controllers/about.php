<?php

class about extends AppController {
    public function __construct($parent) {
        $this->config = $parent->config;
        $this->parent = $parent;
        $this->urlPathParts = $parent->urlPathParts;
        $this->showList();
    }

    public function showList() {
        
        $data = $this->parent->getModel("fruits")->select(
            "SELECT * FROM fruit_table"
        );
        $this->getView("header", array("pagename" => "About", "config" => $this->config["navigation"], "pathparts" => $this->urlPathParts));
        $this->getView("about", $data);
        $this->getView("footer");
    }

    public function showAddForm() {
        $this->getView("header", array("pagename" => "About", "config" => $this->config["navigation"], "pathparts" => $this->urlPathParts));
        $this->getView("addForm");
        $this->getView("footer");
    }

    public function addAction() {
        $this->parent->getModel("fruits")->add(
            "INSERT INTO fruit_table (name) values(:name)",
            array(":name" => $_REQUEST["name"])
        );

        header("Location:/about");
    }

    public function edit() {

        $data = $this->parent->getModel("fruits")->select(
            "SELECT * FROM fruit_table WHERE id = :id LIMIT 1",
            array(":id" => $_REQUEST["id"])
        );

        $this->getView("header", array("pagename" => "About", "config" => $this->config["navigation"], "pathparts" => $this->urlPathParts));
        $this->getView("editForm", $data);
        $this->getView("footer");
    }

    public function editAction() {
        $this->parent->getModel("fruits")->update(
            "UPDATE fruit_table SET name = :name WHERE id = :id",
            array(":name" => $_REQUEST["name"], ":id" => $_REQUEST["id"])
        );

        header("Location:/about");
    }

    public function delete() {
        $this->parent->getModel("fruits")->delete(
            "DELETE FROM fruit_table WHERE id = :id",
            array(":id" => $_REQUEST["id"])
        );

        header("Location:/about");
    }
}

?>