<?php

class api extends AppController {
    public function __construct($parent) {
        $this->config = $parent->config;
        $this->parent = $parent;
        $this->urlPathParts = $parent->urlPathParts;
    }

    public function showApi() {
        $this->getView("header", array("pagename" => "api", "config" => $this->config["navigation"], "pathparts" => $this->urlPathParts));
        $data = $this->parent->getModel("apiModel")->googleBooks("Henry David Thoreau");
        $this->getView("api", $data);
        $this->getView("footer");
    }
}

?>