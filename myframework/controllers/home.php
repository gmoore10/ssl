<?php

class home extends AppController {
    public function __construct($config) {
        $this->config = $config;
    }

    public function index() {
        $this->getView("header", array("pagename" => "Home", "config" => $this->config->config["navigation"], "pathparts" => $this->config->urlPathParts));
        $this->getView("home");
        $this->getView("footer");
    }

    public function contact() {
        $this->getView("header", array("pagename" => "Home", "config" => $this->config->config["navigation"], "pathparts" => $this->config->urlPathParts));
        
        $random = substr( md5(rand()), 0, 7);
        $this->getView("contact", array("cap" => $random));
        
        $this->getView("footer");
    }

    public function contactRecv() {
        $validEmail = false;
        $validPassword = false;

        if($_POST["captcha"] && $_POST["captcha"] == $_SESSION["cap"]) {
            if(!filter_var($_POST["email"], FILTER_VALIDATE_EMAIL)) {
                $validEmail = false;
            } else {
                $validEmail = true;
            }
    
            if($_POST["password"] != "") {
                if(preg_match("/^[a-zA-Z]*$/", $_POST["password"])) {
                    $validPassword = true;
                }
            } else {
                $validPassword = false;
            }
    
            if($validEmail == true && $validPassword == true) {
                $this->getView("header", array("pagename" => "Thanks!", "config" => $this->config->config["navigation"], "pathparts" => $this->config->urlPathParts));
                $this->getView("thanks");
            } else {
                $this->getView("header", array("pagename" => "About", "config" => $this->config->config["navigation"], "pathparts" => $this->config->urlPathParts));
                $random = substr( md5(rand()), 0, 7);
                $this->getView("contact", array("validEmail" => $validEmail, "validPassword" => $validPassword, "cap" => $random));
            }
            $this->getView("footer");
        } else {
            echo "Invalid CAPTCHA entry.";
            echo '<br /><a href="/home/contact">Click here to go back.</a>';
        }
    }

    public function login() {
        

        if(@$_REQUEST["email"] == "mike@aol.com") {
            echo "welcome";
        } else {
            echo "bad login";
        }
    }
}

?>