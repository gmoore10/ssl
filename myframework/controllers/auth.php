<?php

class auth extends AppController {
    public function __construct($parent) {
        //$this->config = $config;
        $this->parent = $parent;
    }

    public function login() {
        if($_REQUEST["username"] && $_REQUEST["password"]) {
            $data = $this->parent->getModel("users")->select(
                "SELECT * FROM users WHERE email = :email AND password = :password",
            array(":email" => $_REQUEST["username"], ":password" => sha1($_REQUEST["password"])));

            if($data) {
                $_SESSION["loggedin"] = 1;
                $_SESSION["profile"] = array("email" => $_REQUEST["username"]);
                header("Location:/home");
            } else {
                header("Location:/home?msg=Bad Login");
            }
        }
    }

    public function logout() {
        session_destroy();
        header("Location:/home");
    }
}

?>