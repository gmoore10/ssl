<?php

class login extends AppController {
    public function __construct($config) {

        $this->config = $config;

        $this->getView("header", array("pagename" => "About", "config" => $config->config["navigation"], "pathparts" => $config->urlPathParts));
        $this->getView("login");
        $this->getView("footer");
    }
}

?>