<?php

class googleLogin extends AppController {
    public function __construct($parent) {
        $this->config = $parent->config;
        $this->parent = $parent;
        $this->urlPathParts = $parent->urlPathParts;
    }

    public function login() {
        $this->getView("header", array("pagename" => "api", "config" => $this->config["navigation"], "pathparts" => $this->urlPathParts));
        $data = $this->parent->getModel("googleLoginModel")->getAuthUrl();
        $this->getView("googleLogin", $data);
        $this->getView("footer");
    }

    public function logout() {
        $this->parent->getModel("googleLoginModel")->logout();        
    }

    public function callback() {
        if(isset($_GET["code"])) {
            $token = $this->parent->getModel("googleLoginModel")->fetchAccessTokenWithAuthCode($_GET["code"]);

            header("Location:/googleLogin/userProfile");
        }

        $this->getView("header", array("pagename" => "api", "config" => $this->config["navigation"], "pathparts" => $this->urlPathParts));
        $data = $this->parent->getModel("googleLoginModel")->getAuthUrl();
        $this->getView("googleLogin", $data);
        $this->getView("footer");
    }

    public function userProfile() {
        $userData = $this->parent->getModel("googleLoginModel")->getUserInfo();
        $this->getView("header", array("pagename" => "User Info", "config" => $this->config["navigation"], "pathparts" => $this->urlPathParts));
        $this->getView("googleProfile", $userData);
        $this->getView("footer");
    }
}

?>