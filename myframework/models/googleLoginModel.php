<?php

class googleLoginModel {
    public function __construct($parent) {
        $this->db = $parent->db;

        require_once '/google-api-php-client/vendor/autoload.php';
        
        $client = new Google_Client();
        $client->setClientId("955493671398-tfqdpdjqih875eucq609c1vnc28bpfui.apps.googleusercontent.com");
        $client->setClientSecret("F53CZXUy7LfzDIRY-rUZcv2n");
        $client->setApplicationName("sslapi");
        $client->setDeveloperKey("AIzaSyCCj5v1l7mATiJXrQvg5ZzVcLbvIMpuPfs");
        $client->setRedirectUri("http://" . $_SERVER["SERVER_NAME"] . "/googleLogin/callback");
        $client->addScope("https://www.googleapis.com/auth/plus.login https://www.googleapis.com/auth/userinfo.email");
        $client->setAccessType('offline');
        $client->setApprovalPrompt('force');

        $this->client = $client;
    }

    public function getAuthUrl() {
        return $this->client->createAuthUrl();
    }

    public function fetchAccessTokenWithAuthCode($code) {
        $this->client->authenticate($code);
        $token = $this->client->getAccessToken($code);
        $_SESSION['access_token'] = $token;

        return $token;
    }

    public function logout() {
        unset($_SESSION["access_token"]);
        $this->client->revokeToken();
        header("Location:/home");
    }

    public function getUserInfo() {
        $this->client->setAccessToken($_SESSION["access_token"]);
        
        $oAuth = new Google_Service_Oauth2($this->client);
        $userData = $oAuth->userinfo_v2_me->get();

        return $userData;
    }
}

?>